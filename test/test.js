'use strict'

// const { describe, it, before, after, afterEach } = require('mocha')
const { describe, before, after, afterEach } = require('mocha')

const { connect } = require('../src/server')
// const { expect } = require('chai')
// const geofences = require('./fixtures/geofences.json')
// const redisData = require('./fixtures/redis.json')
// const devicesData = require('./fixtures/devices.json')
// const data = require('./fixtures/data.json')
const utils = require('../src/utils')
const zmq = require('zeromq/v5-compat')
const Redis = require('ioredis')
// const nock = require('nock')

const redisUri = utils.getRedisUri()
const client = new Redis(redisUri)

describe('geofences', function () {
  this.timeout(20000)
  const push = zmq.socket('push')

  before(done => {
    connect(err => {
      if (err) throw err
      push.bind(utils.getPullParserUri(), err => {
        if (err) throw err
        done()
      })
    })
  })

  afterEach(done => utils.dropFixtures(done))
  after(() => client.flushall())
})
