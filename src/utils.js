'use strict'

const { URL } = require('url')
// const moment = require('moment-timezone')
const turf = require('@turf/turf')
const axios = require('axios')
// const TIME_ZONE = process.env.TIME_ZONE || 'America/Lima'
const NOTIFICATIONS_ENDPOINT =
  process.env.NOTIFICATIONS_ENDPOINT || 'http://127.0.0.1:5000/notificate'
const NOTIFICATIONS_TOKEN = process.env.NOTIFICATIONS_TOKEN || ''

let _db, _client

/*
  Sets db to global use
*/
const setDb = db => {
  _db = db
}

/*
  Sets client to global use
*/
const setClient = client => {
  _client = client
}

/*
  Returns mongodb url
*/
const getMongodbUri = () => {
  let uri = 'mongodb://localhost:27017/covid-1969'
  if (
    process.env.MONGO_1_PORT_27017_TCP_ADDR &&
    process.env.MONGO_1_PORT_27017_TCP_PORT
  ) {
    uri = `mongodb://${process.env.MONGO_1_PORT_27017_TCP_ADDR}:${
      process.env.MONGO_1_PORT_27017_TCP_PORT
    }/covid-1969`
  } else if (
    process.env.MONGO_PORT_27017_TCP_ADDR &&
    process.env.MONGO_PORT_27017_TCP_PORT
  ) {
    uri = `mongodb://${process.env.MONGO_PORT_27017_TCP_ADDR}:${
      process.env.MONGO_PORT_27017_TCP_PORT
    }/covid-1969`
  } else if (process.env.MONGODB_URI) {
    uri = process.env.MONGODB_URI
  }
  return uri
}

/*
  Returns mongodb database name from url
*/
const getMongodbDbname = () => {
  const uri = new URL(getMongodbUri())
  if (!uri && !uri.pathname) return 'test'
  return uri.pathname.substr(1)
}

/*
  Returns Redis url
*/
const getRedisUri = () => {
  let uri = 'redis://localhost:6379'
  if (
    process.env.REDIS_1_PORT_6379_TCP_ADDR &&
    process.env.REDIS_1_PORT_6379_TCP_PORT
  ) {
    uri = `redis://${process.env.REDIS_1_PORT_6379_TCP_ADDR}:${
      process.env.REDIS_1_PORT_6379_TCP_PORT
    }`
  } else if (
    process.env.REDIS_PORT_6379_TCP_ADDR &&
    process.env.REDIS_PORT_6379_TCP_PORT
  ) {
    uri = `redis://${process.env.REDIS_PORT_6379_TCP_ADDR}:${
      process.env.REDIS_PORT_6379_TCP_PORT
    }`
  } else if (process.env.REDIS_URI) {
    uri = process.env.REDIS_URI
  }
  return uri
}

/*
  Returns Zeromq Pull from TCP url
*/
const getPullTcpUri = () => {
  let uri = 'tcp://127.0.0.1:3335'
  if (process.env.ZMQ_PULL_URI) {
    uri = process.env.ZMQ_PULL_URI
  }
  return uri
}

/*
  Returns true if data point is inside the geofence
*/
const isInsideGeofence = (data, cb) => {
  const collection = _db.collection('geofences')
  const query = {
    user_id: data.userId,
    loc: { $geoIntersects: { $geometry: data.point } }
  }
  collection.find(query).toArray((err, docs) => {
    if (err) cb(err, null)
    cb(null, docs.length !== 0)
  })
}

/*
  Saves in Redis already notificated
*/
const addAlreadyNotificated = (userId, notificated, cb) => {
  const key = `users:${userId}`
  _client
    .hset(key, 'notificated', notificated)
    .then(() => {
      cb(null, true)
    })
    .catch(cb)
}

/*
  Loc validators
*/
const isLatitude = lat => lat >= -90 && lat <= 90
const isLongitude = lng => lng >= -180 && lng <= 180
const hasLoc = (lat, lng) => lat !== 0 && lng !== 0

/*
  Validates that lat and lng are valid values.
*/
const validateCoordinates = loc => {
  const lat = loc.coordinates[1]
  const lng = loc.coordinates[0]
  return isLatitude(lat) && isLongitude(lng) && hasLoc(lat, lng)
}

/*
  Calculates distance between two points
  from -> [lng, lat]
  to -> [lng, lat]
*/
const getDistance = (from, to, unit = 'kilometers') => {
  const fromAsTurf = turf.point(from)
  const toAsTurf = turf.point(to)
  const options = { units: unit }
  return turf.distance(fromAsTurf, toAsTurf, options) * 1000
}

/*
  Returns data in notification format
*/
const getNotificationData = (data, type, message) => {
  const msg = {
    userId: data.userId,
    userType: data.userType,
    notificationType: type,
    message: message
  }
  return JSON.stringify(msg)
}

/*
  Sends Request with Axios to notification server
*/

const sendNotification = data => {
  const postData = getNotificationData(data)
  return new Promise((resolve, reject) => {
    axios
      .post(NOTIFICATIONS_ENDPOINT, postData, {
        headers: { Authorization: `Bearer ${NOTIFICATIONS_TOKEN}` }
      })
      .then(res => {
        resolve(res)
      })
      .catch(error => {
        reject(error)
      })
  })
}

module.exports = {
  setDb: setDb,
  setClient: setClient,
  getMongodbUri: getMongodbUri,
  getMongodbDbname: getMongodbDbname,
  getRedisUri: getRedisUri,
  getPullTcpUri: getPullTcpUri,
  validateCoordinates: validateCoordinates,
  getDistance: getDistance,
  isInsideGeofence: isInsideGeofence,
  sendNotification: sendNotification,
  addAlreadyNotificated: addAlreadyNotificated
}
