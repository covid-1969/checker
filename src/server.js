'use strict'

const { EventEmitter } = require('events')
const utils = require('./utils')
const { MongoClient } = require('mongodb')
const Redis = require('ioredis')
const zmq = require('zeromq/v5-compat')

const pull = zmq.socket('pull')

const event = new EventEmitter()

const connect = done => {
  /** ***** REDIS ********/
  const redisUri = utils.getRedisUri()
  const client = new Redis(redisUri)

  utils.setClient(client)
  client.on('error', err => {
    event.emit('error', `Error in Redis: ${err}`)
    done(err)
  })
  client.on('connect', () =>
    event.emit('info', `Connected to Redis in ${redisUri}`)
  )

  /** ***** MONGO ********/
  const mongodbUri = utils.getMongodbUri()
  MongoClient.connect(
    mongodbUri,
    { useNewUrlParser: true },
    (err, mongoClient) => {
      if (err) {
        event.emit('error', `Error connecting to mongo: ${err}`)
        return done(err)
      }
      const db = mongoClient.db(utils.getMongodbDbname())
      utils.setDb(db)
      event.emit('info', `Connected to Mongodb in ${mongodbUri}`)

      /** ***** ZEROMQ ********/
      pull.connect(utils.getPullTcpUri())
      event.emit(
        'info',
        `Socket PULL from TCP connected to ${utils.getPullTcpUri()}`
      )

      // New data received
      pull.on('message', (type, raw) => {
        type = type.toString()
        raw = typeof raw !== 'undefined' ? raw.toString() : null
        if (raw === null) return

        if (type === 'data') {
          const data = JSON.parse(raw)
          client.exists(`geofences:${data.userId}`).then(exists => {
            if (exists && data.point) {
              if (utils.validateCoordinates(data.point)) {
                client
                  .hget(`users:${data.userId}`, 'geofences_query_loc')
                  .then(result => {
                    if (
                      !result ||
                      utils.getDistance(
                        [
                          parseFloat(result.split(',')[0]),
                          parseFloat(result.split(',')[1])
                        ],
                        data.point.coordinates
                      ) > 2
                    ) {
                      utils.isInsideGeofence(data, (err, inside) => {
                        if (err) {
                          event.emit('error', err)
                        } else {
                          if (!inside) {
                            event.emit(
                              'info',
                              `userId ${data.userId} is outside his geofence!`
                            )
                            utils
                              .sendNotification(data)
                              .then(res => {
                                event.emit(
                                  'info',
                                  `Notificated to authorities -> ${res}`
                                )
                                // TODO: Flag as notificated
                              })
                              .catch(err => {
                                event.emit(
                                  'warn',
                                  `Error sending notification -> ${err}`
                                )
                              })
                          }
                          client.hset(
                            `users:${data.userId}`,
                            'geofences_query_loc',
                            `${data.point.coordinates[0]},${
                              data.point.coordinates[1]
                            }`
                          )
                        }
                      })
                    }
                  })
              } else {
                event.emit(
                  'warn',
                  `User ${data.userId} with invalid coordinates.`
                )
              }
            } else {
              event.emit('warn', `User ${data.userId} does not have geofence.`)
            }
          })
        }
      })

      done()
    }
  )
}

module.exports = {
  event: event,
  connect: connect
}
